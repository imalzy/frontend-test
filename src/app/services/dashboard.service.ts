import { userInterface } from './../models/user.interface';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { UserSessionService } from './user-session.service';

@Injectable({
  providedIn: 'root',
})
export class DashboardService {
  constructor(
    private httpClient: HttpClient,
    private router: Router,
    private userSession: UserSessionService,
  ) {}

  getPosts(): Observable<any> {
    return this.httpClient.get(`${environment.api}/posts`).pipe(
      map((response) => {
        return response;
      }),
    );
  }
}
