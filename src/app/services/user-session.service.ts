import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject, Observable } from 'rxjs';
import { map, tap, mapTo, catchError } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { userInterface } from '../models';

@Injectable({
  providedIn: 'root',
})
export class UserSessionService {
  private user: any[] = [];
  private loginStatus$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  private loggedUser: string = null;
  constructor(private httpClient: HttpClient, private router: Router) {}

  isLoggedIn() {
    return !!this.getUserData();
  }

  get loginStatus() {
    return this.loginStatus$.asObservable();
  }

  getAll(): Observable<any> {
    return this.httpClient.get(`${environment.api}/users`).pipe(
      map((response: userInterface[]) => {
        return response;
      }),
    );
  }

  getDetailUser(userId: string | number): Observable<any> {
    return this.httpClient.get(`${environment.api}/users/${userId}`).pipe(
      map((response: userInterface) => {
        return response;
      }),
    );
  }

  authLogin(username: string) {
    this.getAll().subscribe((response) => {
      if (response.length > 0) {
        const result = response.find((e) => e.username.toLowerCase() === username.toLowerCase());
        this.doLogin(result);
        this.router.navigate(['/dashboard']);
      }
    });
  }

  private doLogin(user: userInterface) {
    this.storeUserData(user);
    this.loggedUser = user.username;
    this.loginStatus$.next(true);
  }

  private storeUserData(user: userInterface): void {
    localStorage.setItem('USER', JSON.stringify(user));
  }

  getUserData(): userInterface {
    return JSON.parse(localStorage.getItem('USER'));
  }

  private removeSession() {
    localStorage.clear();
  }

  logout() {
    this.loggedUser = null;
    this.removeSession();
    this.loginStatus$.next(false);
    this.router.navigate(['/login']);
  }
}
