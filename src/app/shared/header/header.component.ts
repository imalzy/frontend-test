import { userInterface } from './../../models/user.interface';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Observable } from 'rxjs';
import { UserSessionService } from 'src/app/services/user-session.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {
  user: userInterface;
  @Output() loginBtn = new EventEmitter();
  isLoggedIn$: Observable<boolean>;
  status: boolean = false;

  constructor(private userSession: UserSessionService, private router: Router) {}

  ngOnInit(): void {
    this.user = this.userSession.getUserData();
    this.isLoggedIn$ = this.userSession.loginStatus;
  }

  onLogin(): void {
    this.status = true;
    this.loginBtn.emit({ status: this.status });
  }

  onProfile(): void {
    this.router.navigateByUrl(`/dashboard/profile?userId=${this.user.id}`);
  }
}
