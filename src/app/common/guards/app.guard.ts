import { Injectable } from '@angular/core';
import { CanActivate, CanLoad, Router } from '@angular/router';
import { UserSessionService } from 'src/app/services/user-session.service';

@Injectable({
  providedIn: 'root',
})
export class AppGuard implements CanActivate, CanLoad {
  constructor(private authService: UserSessionService, private router: Router) {}

  canActivate() {
    return this.canLoad();
  }

  canLoad() {
    if (!this.authService.isLoggedIn()) {
      this.router.navigate(['/login']);
    }
    return this.authService.isLoggedIn();
  }
}
