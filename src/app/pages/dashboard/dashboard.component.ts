import { UserSessionService } from 'src/app/services/user-session.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { collectData } from 'src/app/models/dashboard.model';
import { DashboardService } from 'src/app/services/dashboard.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent implements OnInit {
  p: number = 1;
  finalData: any;
  constructor(
    private dashboardService: DashboardService,
    private userSession: UserSessionService,
    private router: Router,
  ) {}

  ngOnInit(): void {
    this.fetchData();
  }

  fetchData(): void {
    this.userSession.getAll().subscribe((users) => {
      if (users.length > 0) {
        this.dashboardService.getPosts().subscribe((posts) => {
          if (posts.length > 0) {
            this.finalData = collectData(posts, users);
            console.log(this.finalData);
          }
        });
      }
    });
  }
}
