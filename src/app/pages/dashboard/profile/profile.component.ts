import { userInterface } from './../../../models/user.interface';
import { UserSessionService } from 'src/app/services/user-session.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
})
export class ProfileComponent implements OnInit {
  detailUser: userInterface;
  constructor(private router: Router, private user: UserSessionService) {}

  ngOnInit(): void {
    //     this.actRoute.paramMap.subscribe((params: ParamMap) => {
    //       const userId = params.get('userId');
    //       console.log(userId)
    // });

    // const userId = this.route.snapshot.paramMap.get('userId');
    // console.log(userId);

    const userId = this.getQueryStringValue('userId');
    this.user.getDetailUser(userId).subscribe((detail) => {
      this.detailUser = detail;
    });
  }

  onBack(): void {
    this.router.navigateByUrl('/dashboard');
  }

  getQueryStringValue(key) {
    return decodeURIComponent(
      window.location.search.replace(
        new RegExp(
          '^(?:.*[&\\?]' +
            encodeURIComponent(key).replace(/[\.\+\*]/g, '\\$&') +
            '(?:\\=([^&]*))?)?.*$',
          'i',
        ),
        '$1',
      ),
    );
  }
}
