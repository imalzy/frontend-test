import { UserSessionService } from './../../services/user-session.service';
import { FormControl, FormGroup } from '@angular/forms';
import { trigger, transition, style, animate } from '@angular/animations';
import { Component, OnInit } from '@angular/core';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  animations: [
    trigger('fadeIn', [
      transition('void => *', [style({ opacity: 0 }), animate(2000, style({ opacity: 1 }))]),
    ]),
  ],
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  visible: boolean = false;
  constructor(private userSession: UserSessionService) {}

  ngOnInit(): void {
    this.loginForm = new FormGroup({
      username: new FormControl(''),
      password: new FormControl(''),
    });
  }

  onLogin(event: { status: boolean }): void {
    if (event.status === true) {
      this.visible = true;
    }
  }
  onSubmit(): void {
    this.userSession.authLogin(this.loginForm.get('username').value);
  }
}
