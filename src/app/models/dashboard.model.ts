import { userInterface } from '.';

export function collectData(dataPosts: postInterface[], users: userInterface[]) {
  let posts = [];
  for (let i = 0; i < dataPosts.length; i++) {
    posts.push({
      ...dataPosts[i],
      ...users.find((user) => user.id === dataPosts[i].userId),
    });
  }
  return posts;
}

export interface postInterface {
  body: string;
  id: number;
  title: string;
  userId: number;
}
